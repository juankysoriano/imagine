package com.juankysoriano.rainbow.core.exeption;

public class RainbowLifeCycleException extends RuntimeException {

    public RainbowLifeCycleException(String error) {
        super(error);
    }
}
